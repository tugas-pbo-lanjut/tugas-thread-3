package thread;

public class MutlipleThreadsRunnable {
    public static void main(String[] args) {
        MyRunnable runnable1 = new MyRunnable();
        MyRunnable runnable2 = new MyRunnable();
        Thread t1 = new Thread(runnable1);
        t1.setName("Thread l1");
        Thread t2 = new Thread(runnable2);
        t2.setName("Thread l2");
        Thread t3 = new Thread(runnable1);
        t3.setName("Thread l3");
        Thread t4 = new Thread(runnable1);
        t4.setName("Thread l4");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}