package thread;

class MyRunnable implements Runnable {
    int var = 0;
    @Override
    public void run() {
        call();
    }

    public void call() {
        synchronized (this) {
            for (int i = 0; i < 4; i++) {
                System.out.println("Thread sekarang adalah " + Thread.currentThread().getName() + " yang bernilai "+var);
                var++;
            }
        }
    }
}